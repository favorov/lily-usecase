#!/bin/bash
#this 3 variables are run-dependent
name=1_h3k27ac
input=1_2percent_input_bowtie2_hg19_sort.bam
signal=1_h3k27ac_bowtie2_hg19_sort.bam
#they are
bam_folder=ChipSeqHPVHNSCC-6_samples/bams/
processed_bam_folder=processed-bams/
current_folder=`pwd`
mkdir -p ${processed_bam_folder}
#remove duplicates from bams
[ ! -f ${processed_bam_folder}/${input} ] && samtools view -u -q 20 ${bam_folder}/${input} | samtools rmdup -s - ${processed_bam_folder}/${input}.tmp && mv ${processed_bam_folder}/${input}.tmp ${processed_bam_folder}/${input}
[ ! -f ${processed_bam_folder}/${signal} ] && samtools view -u -q 20 ${bam_folder}/${signal} | samtools rmdup -s - ${processed_bam_folder}/${signal}.tmp && mv ${processed_bam_folder}/${signal}.tmp ${processed_bam_folder}/${signal}
#prepare directory
mkdir -p ${name}
#peak calling
[ ! -f ${name}/${name}.wig ] && ./hmcan/src/HMCan ${processed_bam_folder}/${signal} ${processed_bam_folder}/${input} HMCan.config.narrow.25GC.txt ${name}/${name}
#stich and sort enhancers
#[ ! -f  ${name}/${name}.scores.bed ] && cat Lilly/LILY/scripts/runLILY.R | R --slave --args ~/lilly-revisited/${name}/${name} ${name} 12500 2500 ${current_folder}/hg19_refseq.ucsc ${current_folder}/hg19.fa.fai
[ ! -f  ${name}/${name}.scores.bed ] && cat ./LILY/scripts/runLILY.R | R --slave --args ${current_folder}/${name}/${name} ${name} 12500 2500 ${current_folder}/hg19_refseq.ucsc ${current_folder}/hg19.fa.fai
