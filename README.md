# LILY usecase
LILY is an an instrument for identification of enhancers in H3K27ac data 
https://github.com/BoevaLab/LILY

This usecase is for 047 HPV+ HNSCC cancer line H3K27ac CHiP-Seq data by D. Gaykalova lab.

It is critical to use the same genome as was used for the alignmenent of input BAMs.

The instructions to generate GC content profile for hg38 are on LILY webpage.

